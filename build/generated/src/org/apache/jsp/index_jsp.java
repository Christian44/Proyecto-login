package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\r\n");
      out.write("<style>\r\n");
      out.write(" body{\r\n");
      out.write("\t\r\n");
      out.write("        background:green;\r\n");
      out.write("\tbackground-size: 100vw 100vh;\r\n");
      out.write("\tbackground-attachment: fixed;\r\n");
      out.write("\tmargin: 0;\r\n");
      out.write("     \r\n");
      out.write("}\r\n");
      out.write(" h2{\r\n");
      out.write("\tcolor: #fff;\r\n");
      out.write("\ttext-align: center;\r\n");
      out.write("\tmargin: 0;\r\n");
      out.write("\tfont-size: 30px;\r\n");
      out.write("\tmargin-bottom: 20px;\r\n");
      out.write("}  \r\n");
      out.write("\r\n");
      out.write("#form1{\r\n");
      out.write("     width: 450px;\r\n");
      out.write("     margin: auto;\r\n");
      out.write("     background: rgba(0,0,0,0.4);\r\n");
      out.write("     padding: 10px 28px;\r\n");
      out.write("     margin-top: 20px;\r\n");
      out.write("     border-radius:7px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("input, textarea{\r\n");
      out.write("\twidth: 100%;\r\n");
      out.write("\tmargin-bottom: 20px;\r\n");
      out.write("\tpadding: 7px;\r\n");
      out.write("\tbox-sizing: border-box;\r\n");
      out.write("\tfont-size: 17px;\r\n");
      out.write("\tborder: none;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("#sumit{\r\n");
      out.write("\tbackground: #31384A;\r\n");
      out.write("\tcolor: #fff;\r\n");
      out.write("        min-height: 30px;\r\n");
      out.write("        max-height: 200px;\r\n");
      out.write("        max-width:  100%;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("label{\r\n");
      out.write("    color: #fff;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("@media (max-width: 480px){\r\n");
      out.write("\t#form1{\r\n");
      out.write("\t\twidth: 100%;\r\n");
      out.write("\t}\r\n");
      out.write("}\r\n");
      out.write("</style>\r\n");
      out.write("<title>AJAX JSP Servelts</title>\r\n");
      out.write("<script src=\"http://code.jquery.com/jquery-latest.js\">\r\n");
      out.write("\r\n");
      out.write("</script>\r\n");
      out.write("<script>\r\n");
      out.write("\t$(document).ready(function() {\r\n");
      out.write("\t\t$('#submit').click(function(event) {\r\n");
      out.write("\t\t\tvar usuarioVar = $('#usuario').val();\r\n");
      out.write("\t\t\tvar contrasenaVar = $('#contrasena').val();\r\n");
      out.write("\t\t\t// Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get\r\n");
      out.write("\t\t\t$.post('ActionServlet', {\r\n");
      out.write("\t\t\t\tusuario : usuarioVar,\r\n");
      out.write("\t\t\t\tcontrasena: contrasenaVar,\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t}, function(responseText) {\r\n");
      out.write("\t\t\t\t$('#tabla').html(responseText);\r\n");
      out.write("\t\t\t});\r\n");
      out.write("\t\t});\r\n");
      out.write("\t});\r\n");
      out.write("</script>\r\n");
      out.write("<link rel=\"stylesheet\" href=\"estilos.css\" type=\"text/css\"></link> \r\n");
      out.write("</head>\r\n");
      out.write("<body >\r\n");
      out.write("\t\r\n");
      out.write("\t<form id=\"form1\">\r\n");
      out.write("            <h2> LOGIN SENCILLO </h2>\r\n");
      out.write("\t\t<label> Username : </label><input type=\"text\" id=\"usuario\" /> <br><br>\r\n");
      out.write("\t\t<label> Password : </label> <input type=\"text\" id=\"contrasena\" /> <br>\r\n");
      out.write("                <br><input type=\"button\" id=\"submit\" value=\"Aceptar\" /> \r\n");
      out.write("\t</form>\r\n");
      out.write("\t<br>\r\n");
      out.write("\t<!-- \tEn este div metemos el contenido de la tabla con AJAX -->\r\n");
      out.write("\t<div id=\"tabla\"></div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
