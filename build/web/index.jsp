<%-- 
    Document   : index
    Created on : 05/03/2017, 13:46:30
    Author     : xavier
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<style>
 body{
	
        background:green;
	background-size: 100vw 100vh;
	background-attachment: fixed;
	margin: 0;
     
}
 h2{
	color: #fff;
	text-align: center;
	margin: 0;
	font-size: 30px;
	margin-bottom: 20px;
}  

#form1{
     width: 450px;
     margin: auto;
     background: rgba(0,0,0,0.4);
     padding: 10px 28px;
     margin-top: 20px;
     border-radius:7px;
}

input, textarea{
	width: 100%;
	margin-bottom: 20px;
	padding: 7px;
	box-sizing: border-box;
	font-size: 17px;
	border: none;
}

#sumit{
	background: #31384A;
	color: #fff;
        min-height: 30px;
        max-height: 200px;
        max-width:  100%;
}

label{
    color: #fff;
}


@media (max-width: 480px){
	#form1{
		width: 100%;
	}
}
</style>
<title>AJAX JSP Servelts</title>
<script src="http://code.jquery.com/jquery-latest.js">

</script>
<script>
	$(document).ready(function() {
		$('#submit').click(function(event) {
			var usuarioVar = $('#usuario').val();
			var contrasenaVar = $('#contrasena').val();
			// Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
			$.post('ActionServlet', {
				usuario : usuarioVar,
				contrasena: contrasenaVar,
				
			}, function(responseText) {
				$('#tabla').html(responseText);
			});
		});
	});
</script>
<link rel="stylesheet" href="estilos.css" type="text/css"></link> 
</head>
<body >
	
	<form id="form1">
            <h2> LOGIN SENCILLO </h2>
		<label> Username : </label><input type="text" id="usuario" /> <br><br>
		<label> Password : </label> <input type="text" id="contrasena" /> <br>
                <br><input type="button" id="submit" value="Aceptar" /> 
	</form>
	<br>
	<!-- 	En este div metemos el contenido de la tabla con AJAX -->
	<div id="tabla"></div>
</body>
</html>